import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatOptionModule,
  MatFormFieldModule,
  MatSelectModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SellersComponent } from './sellers/sellers.component';
import { SellersDetailComponent } from './sellers-detail/sellers-detail.component';
import { SellersCreateComponent } from './sellers-create/sellers-create.component';
import { SellersEditComponent } from './sellers-edit/sellers-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './main/main.component';
import { ItemsComponent } from './items/items.component';
import { ItemsDetailComponent } from './items-detail/items-detail.component';
import { ItemsCreateComponent } from './items-create/items-create.component';
import { ItemsEditComponent } from './items-edit/items-edit.component';
import { PhotosComponent } from './photos/photos.component';
import { PhotosCreateComponent } from './photos-create/photos-create.component';
import { PhotosDetailComponent } from './photos-detail/photos-detail.component';
import { PhotosEditComponent } from './photos-edit/photos-edit.component';
import { VendorsComponent } from './vendors/vendors.component';
import { VendorsCreateComponent } from './vendors-create/vendors-create.component';
import { VendorsDetailComponent } from './vendors-detail/vendors-detail.component';
import { VendorsEditComponent } from './vendors-edit/vendors-edit.component';
import { ListingsComponent } from './listings/listings.component';
import { ListingsCreateComponent } from './listings-create/listings-create.component';
import { ListingsDetailComponent } from './listings-detail/listings-detail.component';
import { ListingsEditComponent } from './listings-edit/listings-edit.component';
import { HelpComponent } from './help/help.component';
import { GetItemNamePipe } from './get-item-name.pipe';

const appRoutes: Routes = [
  {
    path: 'sellers',
    component: SellersComponent,
    data: { title: 'Sellers List' }
  },
  {
    path: 'sellers-details/:id',
    component: SellersDetailComponent,
    data: { title: 'Sellers Details' }
  },
  {
    path: 'sellers-create',
    component: SellersCreateComponent,
    data: { title: 'Create Sellers' }
  },
  {
    path: 'sellers-edit/:id',
    component: SellersEditComponent,
    data: { title: 'Edit Sellers' }
  },
  {
    path: 'items',
    component: ItemsComponent,
    data: { title: 'Items List' }
  },
  {
    path: 'items-details/:id',
    component: ItemsDetailComponent,
    data: { title: 'Item Details' }
  },
  {
    path: 'items-create',
    component: ItemsCreateComponent,
    data: { title: 'Create Item' }
  },
  {
    path: 'items-edit/:id',
    component: ItemsEditComponent,
    data: { title: 'Edit Items' }
  },
  {
    path: 'photos',
    component: PhotosComponent,
    data: { title: 'Photos List' }
  },
  {
    path: 'photos-details/:id',
    component: PhotosDetailComponent,
    data: { title: 'Photos Details' }
  },
  {
    path: 'photos-create',
    component: PhotosCreateComponent,
    data: { title: 'Create photo' }
  },
  {
    path: 'photos-edit/:id',
    component: PhotosEditComponent,
    data: { title: 'Edit photo' }
  },
  {
    path: 'vendors',
    component: VendorsComponent,
    data: { title: 'Vendors List' }
  },
  {
    path: 'vendors-details/:id',
    component: VendorsDetailComponent,
    data: { title: 'Vendors Details' }
  },
  {
    path: 'vendors-create',
    component: VendorsCreateComponent,
    data: { title: 'Create vendor' }
  },
  {
    path: 'vendors-edit/:id',
    component: VendorsEditComponent,
    data: { title: 'Edit vendor' }
  },
  {
    path: 'listings',
    component: ListingsComponent,
    data: { title: 'Listings List' }
  },
  {
    path: 'listings-details/:id',
    component: ListingsDetailComponent,
    data: { title: 'Listings Details' }
  },
  {
    path: 'listings-create',
    component: ListingsCreateComponent,
    data: { title: 'Create listing' }
  },
  {
    path: 'listings-edit/:id',
    component: ListingsEditComponent,
    data: { title: 'Edit listing' }
  },
  {
    path: 'main',
    component: MainComponent,
    data: { title: 'Main Menu' }
  },
  {
    path: 'help',
    component: HelpComponent,
    data: { title: 'Help Menu' }
  },
  {
    path: '',
    redirectTo: '/main',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    SellersComponent,
    SellersDetailComponent,
    SellersCreateComponent,
    SellersEditComponent,
    MainComponent,
    ItemsComponent,
    ItemsDetailComponent,
    ItemsCreateComponent,
    ItemsEditComponent,
    PhotosComponent,
    PhotosCreateComponent,
    PhotosDetailComponent,
    PhotosEditComponent,
    VendorsComponent,
    VendorsCreateComponent,
    VendorsDetailComponent,
    VendorsEditComponent,
    ListingsComponent,
    ListingsCreateComponent,
    ListingsDetailComponent,
    ListingsEditComponent,
    HelpComponent,
    GetItemNamePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    AppRoutingModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

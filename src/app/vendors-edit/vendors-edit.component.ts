import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-vendors-edit',
  templateUrl: './vendors-edit.component.html',
  styleUrls: ['./vendors-edit.component.css']
})
export class VendorsEditComponent implements OnInit {

  id = '';

  vendorsForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getVendor(this.route.snapshot.params['id']);
    this.vendorsForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'saleCut': [null, Validators.required],
      'listCut': [null, Validators.required],
    });
  }

  getVendor(id) {
    this.fs.getVendor(id).subscribe(data => {
      this.id = data.key;
      this.vendorsForm.setValue({
        name: data.name,
        saleCut: data.saleCut,
        listCut: data.listCut
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.updateVendors(this.id, form)
      .subscribe(res => {
        this.router.navigate(['/vendors']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  vendorsDetails() {
    this.router.navigate(['/vendors-details', this.id]);
  }

}


import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-items-edit',
  templateUrl: './items-edit.component.html',
  styleUrls: ['./items-edit.component.css']
})
export class ItemsEditComponent implements OnInit {

  itemsForm: FormGroup;

  sellers: Array<Seller>;

  id = '';

  constructor(private router: Router, private route: ActivatedRoute, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sellers = this.fs.getSellersJson();
    this.getItem(this.route.snapshot.params['id']);
    this.itemsForm = this.formBuilder.group({
      'sellerId': [null, Validators.required],
      'desc': [null, Validators.required],
      'qoh': [null, Validators.required],
      'buy': [null, Validators.required],
      'sell': [null, Validators.required],
      'ship': [null, Validators.required],
      'tax': [null, Validators.required],
    });
  }

  getItem(id) {
    this.fs.getItem(id).subscribe(data => {
      this.id = data.key;
      this.itemsForm.setValue({
        sellerId: data.sellerId,
        desc: data.desc,
        qoh: data.qoh,
        buy: data.buy,
        sell: data.sell,
        ship: data.ship,
        tax: data.tax,
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.updateItems(this.id, form)
      .subscribe(res => {
        this.router.navigate(['/items']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  itemsDetails() {
    this.router.navigate(['/items-details', this.id]);
  }

}

export interface Seller {
  sellerId: String;
  desc: String;
  qoh: String;
  buy: String;
  sell: String;
  ship: String;
  tax: String;
}

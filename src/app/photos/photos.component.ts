import { Component, OnInit } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { FsService } from '../fs.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {

  displayedColumns = ['itemId', 'path'];
  dataSource;

  photo;

  constructor(private fs: FsService) {
  }

  ngOnInit() {
    this.fs.getPhotos().subscribe((data: Array<Photo>) => {
      this.photo = data;
      this.dataSource = new MatTableDataSource(this.photo);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

export class PhotoDataSource extends DataSource<any> {

  constructor(private fs: FsService) {
    super();
  }

  connect() {
    return this.fs.getPhotos();
  }

  disconnect() {

  }
}

export interface Photo {
  itemId: String;
  path: String;
}

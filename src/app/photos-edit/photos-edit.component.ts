import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-photos-edit',
  templateUrl: './photos-edit.component.html',
  styleUrls: ['./photos-edit.component.css']
})
export class PhotosEditComponent implements OnInit {

  photosForm: FormGroup;

  items;

  id = '';

  constructor(private router: Router, private route: ActivatedRoute, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.items = this.fs.getItemsJson();
    this.getPhoto(this.route.snapshot.params['id']);
    this.photosForm = this.formBuilder.group({
      'itemId': [null, Validators.required],
      'path': [null, Validators.required],
    });
  }

  getPhoto(id) {
    this.fs.getPhoto(id).subscribe(data => {
      this.id = data.key;
      this.photosForm.setValue({
        itemId: data.itemId,
        path: data.path
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.updatePhotos(this.id, form)
      .subscribe(res => {
        this.router.navigate(['/photos']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  photosDetails() {
    this.router.navigate(['/photos-details', this.id]);
  }

}


import { Component, OnInit } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { FsService } from '../fs.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.css']
})
export class VendorsComponent implements OnInit {

  displayedColumns = ['name', 'saleCut', 'listCut'];
  dataSource;

  vendor;

  constructor(private fs: FsService) {
  }

  ngOnInit() {
    this.fs.getVendors().subscribe((data: Array<Vendor>) => {
      this.vendor = data;
      this.dataSource = new MatTableDataSource(this.vendor);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

export class VendorDataSource extends DataSource<any> {

  constructor(private fs: FsService) {
    super();
  }

  connect() {
    return this.fs.getVendors();
  }

  disconnect() {

  }
}

export interface Vendor {
  name: String;
  saleCut: String;
  listCut: String;
}


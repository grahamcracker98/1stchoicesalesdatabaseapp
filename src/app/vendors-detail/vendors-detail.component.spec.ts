import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsDetailComponent } from './vendors-detail.component';

describe('VendorsDetailComponent', () => {
  let component: VendorsDetailComponent;
  let fixture: ComponentFixture<VendorsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FsService } from '../fs.service';

@Component({
  selector: 'app-vendors-detail',
  templateUrl: './vendors-detail.component.html',
  styleUrls: ['./vendors-detail.component.css']
})
export class VendorsDetailComponent implements OnInit {

  vendor = {};

  constructor(private route: ActivatedRoute, private router: Router, private fs: FsService) { }

  ngOnInit() {
    this.getVendorDetails(this.route.snapshot.params['id']);
  }

  getVendorDetails(id) {
    this.fs.getVendor(id)
      .subscribe(data => {
        this.vendor = data;
      });
  }

  deleteVendor(id) {
    this.fs.deleteVendors(id)
      .subscribe(res => {
        this.router.navigate(['/vendors']);
      }, (err) => {
        console.log(err);
      }
      );
  }

}

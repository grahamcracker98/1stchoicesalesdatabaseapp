import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import * as firebase from 'firebase';
import firestore from 'firebase/firestore';

const settings = { timestampsInSnapshots: true };
const config = {
  apiKey: 'AIzaSyBZr-5Mva0qcpmQvWij-A5woq77RHt7IPQ',
  authDomain: 'firstchoicesales-database.firebaseapp.com',
  databaseURL: 'https://firstchoicesales-database.firebaseio.com',
  projectId: 'firstchoicesales-database',
  storageBucket: 'firstchoicesales-database.appspot.com',
  messagingSenderId: '563679394656'
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = '1st Choice Sales Database';

  public constructor(titleService: Title) {
    titleService.setTitle(this.title);
  }

  ngOnInit() {
    firebase.initializeApp(config);
    firebase.firestore().settings(settings);
  }
}
